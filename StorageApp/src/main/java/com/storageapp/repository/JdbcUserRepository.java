package com.storageapp.repository;

import com.storageapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcUserRepository implements UserRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int put(User user){
        return jdbcTemplate.update(
                "INSERT INTO users(name,password,role,currentstorageid) " +
                        "VALUES('" + user.getName() + "','" + user.getPassword() + "','" + user.getRole() +"','" + user.getCurrentStorageId() +"')");
    }
    public int changeStorage(String storageId, long userId){
        return jdbcTemplate.update(
                "UPDATE users SET currentstorageid='" + storageId + "' WHERE id=" + userId);
    }
    public User retrieve(String name, String password){
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT * FROM users " +
                            "WHERE name='" + name + "' AND password='" + password + "'", BeanPropertyRowMapper.newInstance(User.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }
    public List<User> getUsers(){
        try {
            return jdbcTemplate.query(
                    "SELECT * FROM users", BeanPropertyRowMapper.newInstance(User.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }
}

package com.storageapp.repository;

import com.storageapp.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcItemRepository implements ItemRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int put(Item item){
        return jdbcTemplate.update("" +
                        "INSERT INTO items(name,picture,serial,length,width,height,storage_id,user_id) " +
                            "VALUES(" +
                "'" + item.getName() + "'," +
                "'" + item.getPicture() + "'," +
                (item.getSerial()==""?"NULL" : "'" + item.getSerial() + "'") + "," +
                (item.getLength()==""?"NULL" : "'" + item.getLength() + "'") + "," +
                (item.getWidth()==""?"NULL" : "'" + item.getWidth() + "'") + "," +
                (item.getHeight()==""?"NULL" : "'" + item.getHeight() + "'") + ",'" +
                item.getStorageId() + "','" +
                item.getUserId() + "')");
    }
    /*public List<Item> getItems() {

        try {
            return jdbcTemplate.query("SELECT * FROM items", BeanPropertyRowMapper.newInstance(Item.class));
        }catch(EmptyResultDataAccessException e){
            return null;
        }
    }*/
    public List<Item> getItems(String storageId) {

        try {
            return jdbcTemplate.query("SELECT * FROM items WHERE storage_id='" + storageId + "'", BeanPropertyRowMapper.newInstance(Item.class));
        }catch(EmptyResultDataAccessException e){
            return null;
        }
    }
    public List<Item> getRootItems() {

        try {
            return jdbcTemplate.query("SELECT * FROM items WHERE storage_id is NULL", BeanPropertyRowMapper.newInstance(Item.class));
        }catch(EmptyResultDataAccessException e){
            return null;
        }
    }
    public Item retrieve(long id) {

        try {
            return jdbcTemplate.queryForObject("SELECT * FROM items WHERE id=" + id, BeanPropertyRowMapper.newInstance(Item.class));
        }catch(EmptyResultDataAccessException e){
            return null;
        }
    }
}

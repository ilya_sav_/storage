package com.storageapp.repository;

import com.storageapp.model.Parent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcParentRepository  implements ParentRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Parent> getParents(String userId){

        try {
            return jdbcTemplate.query(
                    "SELECT * FROM getparents('" + userId + "');", BeanPropertyRowMapper.newInstance(Parent.class));
        } catch(EmptyResultDataAccessException | DataIntegrityViolationException e){
            return null;
        }
    }
}

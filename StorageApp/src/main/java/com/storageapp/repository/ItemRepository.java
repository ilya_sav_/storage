package com.storageapp.repository;

import com.storageapp.model.Item;

import java.util.List;

public interface ItemRepository {
    int put(Item item);
    List<Item> getRootItems();
    List<Item> getItems(String storageId);
    Item retrieve(long id);
}

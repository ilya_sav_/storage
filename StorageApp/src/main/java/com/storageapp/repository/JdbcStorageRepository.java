package com.storageapp.repository;

import com.storageapp.model.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcStorageRepository implements StorageRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int put(Storage storage){

        return jdbcTemplate.update(
                "INSERT INTO storages(name,number,parent_id,user_id) " +
                        "VALUES('" + storage.getName() + "'," + storage.getNumber() + ",'" + storage.getParentId() +"','" + storage.getUserId() + "')");
    }
    /*public List<Storage> getStorages(long userId){

        try {
            return jdbcTemplate.query(
                    "SELECT * FROM storages WHERE user_id='" + userId + "'", BeanPropertyRowMapper.newInstance(Storage.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }*/
    public List<Storage> getStorages(String parentId){

        try {
            return jdbcTemplate.query(
                    "SELECT * FROM storages WHERE parent_id='" + parentId + "'", BeanPropertyRowMapper.newInstance(Storage.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }
    public List<Storage> getRootStorages(){

        try {
            return jdbcTemplate.query(
                    "SELECT * FROM storages WHERE parent_id is NULL", BeanPropertyRowMapper.newInstance(Storage.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }
    public int getMaxNumber(String parentId, String name){
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT MAX(number) FROM storages WHERE parent_id='" + parentId + "' AND name='" + name + "'", Integer.class);
        } catch(EmptyResultDataAccessException | NullPointerException e){
            return 0;
        }
    }
    public Storage retrieve(String id){

        try {
            return jdbcTemplate.queryForObject(
                    "SELECT * FROM storages WHERE id=" + id, BeanPropertyRowMapper.newInstance(Storage.class));
        } catch(EmptyResultDataAccessException e){
            return null;
        }
    }
}

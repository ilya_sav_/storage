package com.storageapp.repository;

import com.storageapp.model.Storage;

import java.util.List;

public interface StorageRepository {
    int put(Storage storage);
    //List<Storage> getStorages(long userId);
    Storage retrieve(String id);
}

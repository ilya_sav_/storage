package com.storageapp.repository;

import com.storageapp.model.Parent;

import java.util.List;

public interface ParentRepository {

    List<Parent> getParents(String userId);
}

package com.storageapp.repository;

import com.storageapp.model.User;

import java.util.List;

public interface UserRepository {
    int put(User user);
    User retrieve(String name, String password);
    List<User> getUsers();
}

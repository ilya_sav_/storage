package com.storageapp.controller;

import com.storageapp.model.*;
import com.storageapp.repository.JdbcItemRepository;
import com.storageapp.repository.JdbcParentRepository;
import com.storageapp.repository.JdbcStorageRepository;
import com.storageapp.repository.JdbcUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Controller
@RequestMapping("")
@SessionAttributes("account")
public class UserController {

    @Autowired
    JdbcUserRepository jdbcUserRepository;

    @Autowired
    JdbcStorageRepository jdbcStorageRepository;

    @Autowired
    JdbcParentRepository jdbcParentRepository;

    @Autowired
    JdbcItemRepository jdbcItemRepository;

    /*
     * Entering point. Generating information when user visits web site
     * @param model is used to store data for page
     * @param account is used to store user info while session goes on
     *
     */
    @GetMapping
    public String mainPage(Model model, @ModelAttribute("account") Account account){

        String page = "index";
        switch(account.getType()){
            case "customer":
                model.addAllAttributes(
                        getStorageInfo(account.getCurrentStorageId())
                );
                break;
            case "admin":
                model.addAttribute("users", jdbcUserRepository.getUsers());
                page = "admin";
                break;
            default:
                account.setLogin("Guest");
                break;
        }
        return page;
    }

    /*
     * User clicks on storage and browser sends ajax request
     *
     * @param id is number that identifies object
     *
     *
     */
    @PostMapping("/changeStorage")
    public String changeStorage(Model model,
                                @RequestParam(value="storageid") String id,
                                @ModelAttribute("account") Account account){

        jdbcUserRepository.changeStorage(id, account.getId());
        model.addAllAttributes(getStorageInfo(id));
        return "storages";
    }

    /*
     * User clicks on New storage link and browser asks to Enter name of storage
     * When user fills field and clicks button Create browser sends ajax request
     *
     * @param name
     *
     *
     */
    @PostMapping("/createStorage")
    public String createStore(Model model,
                              @RequestParam(value="name") String name,
                              @ModelAttribute("account") Account account){

        int number = jdbcStorageRepository.getMaxNumber(account.getCurrentStorageId(), name) + 1;
        Storage storage = new Storage(name,
                number,
                account.getCurrentStorageId(),
                "" + account.getId());

        jdbcStorageRepository.put(storage);

        model.addAllAttributes(
                getStorageInfo(account.getCurrentStorageId())
        );
        return "storages";
    }

    /*
     * User clicks on New Item link and browser shows form for data
     * When user fills form and clicks button Create browser sends ajax request
     *
     * @param name
     * @param serial
     * @param length
     * @param width
     * @param height
     *
     *
     */
    @PostMapping("/createItem")
    public String createItem(
            Model model,
            @RequestParam(value="name") String name,
            @RequestParam(value="serial") String serial,
            @RequestParam(value="length") String length,
            @RequestParam(value="width") String width,
            @RequestParam(value="height") String height,
            @ModelAttribute("account") Account account){

        Item item = new Item(
                name,
                "file",
                serial,
                length,width,height,
                account.getCurrentStorageId(),
                "" + account.getId());

        jdbcItemRepository.put(item);

        model.addAllAttributes(
                getStorageInfo(account.getCurrentStorageId())
        );
        return "storages";
    }

    /*
     * When generating new page or changing information on page
     * Request from database new data and providing to method called
     * To avoid duplicate code
     * This actions moved to separate method
     * @param id
     *
     */
    public Map<String, Object> getStorageInfo(String id){

        String parentId = "";
        String current = "/";
        Map<String, Object> map = new HashMap<>();
        Storage storage = jdbcStorageRepository.retrieve(id);

        if(storage != null){
            current = "" + storage;
            parentId = storage.getParentId();
            map.put("items", jdbcItemRepository.getItems(id));
            map.put("storagelist", jdbcStorageRepository.getStorages(id));
        }else{
            map.put("items", jdbcItemRepository.getRootItems());
            map.put("storagelist", jdbcStorageRepository.getRootStorages());
        }
        map.put("storages", jdbcParentRepository.getParents(parentId));
        map.put("current", current);
        return map;
    }

    /*
     * When user fills login form and clicks button Login
     * @param sessionStatus is used to end user session if no user with provided name and password found
     *
     *
     */
    @PostMapping("/login")
    public String mainPage(SessionStatus sessionStatus, @ModelAttribute("account") Account account){

        User user = jdbcUserRepository.retrieve(account.getLogin(), encrypt(account.getPassword()));
        if(user == null) sessionStatus.setComplete();
        else {
            account.setType(user.getRole());
            account.setId(user.getId());
            account.setCurrentStorageId(user.getCurrentStorageId());
        }

        return "redirect:";
    }
    @GetMapping("/logout")
    public String logout(SessionStatus sessionStatus){

        sessionStatus.setComplete();
        return "redirect:";
    }

    /*
     * User clicks on Register link and browser shows Registration form
     * When user fills form and clicks button Register
     * @param sessionStatus is used to end user session if data didn't inserted
     *
     *
     */
    @PostMapping("/register")
    public String register(SessionStatus sessionStatus, @ModelAttribute("account") Account account){

        String password = encrypt(account.getPassword());
        User user = new User(account.getLogin(), password, "customer");
        int rows = jdbcUserRepository.put(user);
        if(rows == 0)sessionStatus.setComplete();
        else {
            user = jdbcUserRepository.retrieve(account.getLogin(), password);
            account.setId(user.getId());
            account.setType(user.getRole());
        }

        return "redirect:";
    }

    /*
     * Database doesn't store paswwords
     * Generating a digest for given value before sending
     * @param password
     *
     */
    private String encrypt(String password){

        String salt="Additional word";

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        }catch(NoSuchAlgorithmException e){
            throw new IllegalArgumentException();
        }
        byte[] hex = md.digest((password + salt).getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for(byte b:hex){
            sb.append(String.format("%02x",b));
        }
        return sb.toString();
    }
    @ModelAttribute("account")
    public Account getUser(){
        return new Account();
    }
}

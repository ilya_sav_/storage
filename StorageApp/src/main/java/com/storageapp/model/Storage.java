package com.storageapp.model;

public class Storage {
    long id;
    String name;
    int number;
    String parentId;
    String userId;

    public Storage(){

    }
    public Storage(String name, int number, String parentId, String userId) {
        this.name = name;
        this.number = number;
        this.parentId = parentId;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String toString(){
        return name + "-" + number;
    }
}

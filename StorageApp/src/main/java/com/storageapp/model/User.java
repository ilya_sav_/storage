package com.storageapp.model;

public class User {
    long id;
    String name;
    String password;
    String role;
    String currentStorageId;

    public User(){

    }
    public User(String name, String password, String role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getCurrentStorageId() {
        return currentStorageId;
    }

    public void setCurrentStorageId(String currentStorageId) {
        this.currentStorageId = currentStorageId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

package com.storageapp.model;

import java.util.List;

public class Account {
    long id;
    String login;
    String password;
    String type;
    String currentStorageId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCurrentStorageId() {
        return currentStorageId;
    }

    public void setCurrentStorageId(String currentStorageId) {
        this.currentStorageId = currentStorageId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type==null?"":type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

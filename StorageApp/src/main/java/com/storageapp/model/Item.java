package com.storageapp.model;

public class Item {

    long id;
    String name;
    String picture;
    String serial;
    String length;
    String width;
    String height;
    String storageId;
    String userId;

    public Item(){

    }
    public Item(String name, String picture, String serial, String length, String width, String height, String storageId, String userId) {
        this.name = name;
        this.picture = picture;
        this.serial = serial;
        this.length = length;
        this.width = width;
        this.height = height;
        this.storageId = storageId;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

<a href="/logout">Logout</a><br><br>
Present Location<br>
<c:forEach  items="${storages}" var="storage">
    <span class="link" onclick="changeStorage(${storage.id});">${storage.title}</span>/
</c:forEach>
<span>${current}</span><br><br>

<div id="storagecontent">
    <h2>Storages</h2>
    <c:forEach items="${storagelist}" var="store">
        <span class="link" onclick="changeStorage(${store.id});">${store}</span><br>
    </c:forEach>
    <h2>Items</h2>
    <c:forEach items="${items}" var="item">
        <span class="link" onclick="showItemInfo(${item.id});">${item.name} ${item.length} ${item.width} ${item.height}</span><br>
    </c:forEach>
</div>
<div id="data">
    <h2>Actions/Info</h2>
    <span class="link" id="newstoragetext" onclick="shownewstorage()">New Storage</span>
    <span class="link" id="newitemtext" onclick="shownewitem()">New Item</span><br><br>
    <div id="createstorage">
        Name<br>
        <input type="text" id="newstorage">
        <input type="button" value="Create" onclick="createStorage()">
    </div>
    <div id="createitem">
        Name<br>
        <input type="text" id="name"><br>
        Serial number (optional)<br>
        <input type="text" id="serial"><br>
        Length, m (optional)<br>
        <input type="text" id="length"><br>
        Width, m (optional)<br>
        <input type="text" id="width"><br>
        Height, m (optional)<br>
        <input type="text" id="height"><br>
        <input type="button" value="Create" onclick="createItem()"><br>
    </div>
</div>


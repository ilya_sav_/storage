<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Hello ${account.login}!</title>
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.js"></script>
		<script>
		    function changeStorage(storageid){
		        $.ajax({
		            type: 'POST',
		            url: '/changeStorage',
		            data: {storageid:storageid},
		            success: function(result){
		                        $('#content').html(result);
		            }
		        });
		    }
		    function createStorage(){
		        $.ajax({
                    type: 'POST',
                    url: '/createStorage',
                    data: {name:$('#newstorage').val()},
                    success: function(result){
                                $('#content').html(result);
                    }
               });
		    }
		    function showItemInfo(itemid){
		        alert(itemid);
		    }
		    function createItem(){
                $.ajax({
                    type: 'POST',
                    url: '/createItem',
                    data: {
                        name:$('#name').val(),
                        serial:$('#serial').val(),
                        length:$('#length').val(),
                        width:$('#width').val(),
                        height:$('#height').val()},
                    success: function(result){
                                $('#content').html(result);
                    }
               });
            }
            function toggle(){
                $('#registerform,#loginform').toggle();
                var text=$('.link').text();
                $('.link').text(text=="Register"?"Login":"Register");
            }
            function shownewstorage(){
                $('#createstorage,#newitemtext').toggle();
                var text=$('#newstoragetext').text();
                $('#newstoragetext').text(text=="New Storage"?"Hide Form":"New Storage");
            }
            function shownewitem(){
                $('#createitem,#newstoragetext').toggle();
                var text=$('#newitemtext').text();
                $('#newitemtext').text(text=="New Item"?"Hide Form":"New Item");
            }
		</script>
	</head>
	<body>
	    <div id="contain">
	        <div id="inner">
                <h2>Hello ${account.login}!</h2>
                <c:choose>
                    <c:when test="${account.login != 'Guest'}">
                        <div id="content">
                            <%@include file="storage.jsp"%>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <span class="link" onclick="toggle()">Register</span>
                        <form method="post" action="/login" id="loginform">
                            Login<br>
                            <input type="text" name="login"><br>
                            Password<br>
                            <input type="password" name="password"><br>
                            <input type="submit" value="Login">
                        </form>
                        <br>
                        <form method="post" action="/register" id="registerform">
                            Login<br>
                            <input type="text" name="login"><br>
                            Password<br>
                            <input type="password" name="password"><br>
                            <input type="submit" value="Register">
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
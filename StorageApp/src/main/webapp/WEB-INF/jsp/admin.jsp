<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Hello ${account.login}!</title>
	</head>
	<body>

    	<h2>Hello ${account.login}!</h2><br>
    	<h2>${userList}</h2>
    	<table>
    	<tr>
    	<td>Name</td>
    	<td>Role</td>
    	<td>Enable/Disable</td>
    	</tr>
        <c:forEach items="${users}" var="user">
            <tr>
    	    <td>${user.name}</td>
    	    <td>${user.role}</td>
    	    <td><input type="button" value="Enable"></td>
    	    </tr>
    	</c:forEach>
    	</table>
    	<a href="/logout">Logout</a>
    </body>
</html>
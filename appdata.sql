--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.24
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

-- Started on 2022-06-02 16:54:30 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


CREATE FUNCTION public.getparents(rowid text) RETURNS TABLE(id bigint, title character varying)
    LANGUAGE plpgsql
    AS $_$declare
--rec record;
--tbl table(id bigint,title character varying);
storage_id bigint;
storage_name text;
parent_id text;
rnum integer;
begin
CREATE TEMP TABLE IF NOT EXISTS parents(id bigint,title character varying, num integer);
DELETE FROM parents;
rnum:=1;
LOOP
	--SELECT storages.id,storages.name,parent_id INTO rec FROM storages WHERE storages.id=parent;
	EXECUTE 'SELECT storages.id,CONCAT(storages.name,$1,storages.number),parent_id FROM storages WHERE storages.id=$2'
	INTO storage_id,storage_name,parent_id
	USING '-',rowid::bigint;
	INSERT INTO parents(id,title,num) VALUES(storage_id,storage_name,rnum);
	EXIT WHEN parent_id is null;
	rowid:=parent_id;
	rnum:=rnum+1;
END LOOP;
RETURN query SELECT parents.id,parents.title FROM parents ORDER BY parents.num DESC;
end;
$_$;


ALTER FUNCTION public.getparents(rowid text) OWNER TO "App";

SET default_tablespace = '';



CREATE SEQUENCE public.itemid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999999999
    CACHE 1;


ALTER TABLE public.itemid OWNER TO "App";



CREATE TABLE public.items (
    id bigint DEFAULT nextval('public.itemid'::regclass) NOT NULL,
    name character varying(20),
    picture character varying(15),
    serial character varying(20),
    length character varying(10),
    width character varying(10),
    height character varying(10),
    storage_id character varying,
    user_id character varying
);


ALTER TABLE public.items OWNER TO "App";



CREATE SEQUENCE public.storageid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999
    CACHE 1;


ALTER TABLE public.storageid OWNER TO "App";



CREATE TABLE public.storages (
    id bigint DEFAULT nextval('public.storageid'::regclass) NOT NULL,
    name character varying(20),
    number integer,
    parent_id character varying(20),
    user_id character varying(20)
);


ALTER TABLE public.storages OWNER TO "App";



CREATE SEQUENCE public.userid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999
    CACHE 1;


ALTER TABLE public.userid OWNER TO "App";



CREATE TABLE public.users (
    id bigint DEFAULT nextval('public.userid'::regclass) NOT NULL,
    name character varying(20),
    password character varying(64),
    role character varying(15),
    currentstorageid character varying(20)
);


ALTER TABLE public.users OWNER TO "App";



INSERT INTO public.items (id, name, picture, serial, length, width, height, storage_id, user_id) VALUES (2, 'Cable HDMI', NULL, '1218', '1', '0.01', NULL, '3', NULL);
INSERT INTO public.items (id, name, picture, serial, length, width, height, storage_id, user_id) VALUES (3, 'Cable HDMI', NULL, '1224', '2', '0.01', NULL, '3', NULL);
INSERT INTO public.items (id, name, picture, serial, length, width, height, storage_id, user_id) VALUES (4, 'Router', NULL, '284118', '0.15', '0.15', '0.02', '2', NULL);
INSERT INTO public.items (id, name, picture, serial, length, width, height, storage_id, user_id) VALUES (7, 'Cable', NULL, '1542', '1', '2', '0.02', NULL, NULL);
INSERT INTO public.items (id, name, picture, serial, length, width, height, storage_id, user_id) VALUES (17, 'Cable', 'file', NULL, '2', NULL, NULL, '2', '5');



INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (1, 'space', 1, NULL, NULL);
INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (2, 'storage', 1, '1', NULL);
INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (3, 'shelf', 1, '2', NULL);
INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (4, 'mystorage', 2, '2', '5');
INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (5, 'mystorage', 3, '2', '5');
INSERT INTO public.storages (id, name, number, parent_id, user_id) VALUES (8, 'mystorage', 4, '2', '5');



INSERT INTO public.users (id, name, password, role, currentstorageid) VALUES (12, 'Olaf', '5af2f6183ebf5e0f760739b5a3bd8650a94a31ab7c944bbce7523dfe7cd8a3ad', 'admin', NULL);
INSERT INTO public.users (id, name, password, role, currentstorageid) VALUES (5, 'Gustav', '2a50057e2f33567cf4e440938f750d87a3dce43163187fe1e532bfed7c3509e4', 'customer', '2');



SELECT pg_catalog.setval('public.itemid', 18, true);



SELECT pg_catalog.setval('public.storageid', 9, true);



SELECT pg_catalog.setval('public.userid', 10, true);



ALTER TABLE ONLY public.users
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);



ALTER TABLE ONLY public.a
    ADD CONSTRAINT a_pkey PRIMARY KEY (id);



ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);



ALTER TABLE ONLY public.storages
    ADD CONSTRAINT storages_pkey PRIMARY KEY (id);



REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-06-02 16:54:30 EEST

--
-- PostgreSQL database dump complete
--

